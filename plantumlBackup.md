markdown中にplantUMLを仕込みたいが、markdownのビューワの殆どは表示に対応していない。  
なので、作成した画像ファイルをmarkdownには入れる。  
このファイルは画像作成元のplantUMLを残しておくためのもの。


```puml
title UI部品シーンの実装 PopupMenuUISceneの場合

interface "IAdditiveUISceneState\nUI部品を登場退場の制御を外部から行うためのインターフェイス" as IAdditiveUISceneState {
      + void Entry(object entryInfo)
      + void Exit()
      + IObservable<Unit>: onEntryComplete
      + IObservable<Unit>: onExitComplete
}

class "PopupMenuUIView\nポップアップUIの実装" as PopupMenuUIView {
      + void Entry(object entryInfo)
      + void Exit()
      + void UpdateAmount(int amount, bool canIncrement, bool canDecrement)
      + IObservable<Unit>: onEntryComplete
      + IObservable<Unit>: onExitComplete
      + IObservable<Unit>: onClickIncrement
      + IObservable<Unit>: onClickDecrement
      + IObservable<Unit>: onClickClose
}

note bottom of PopupMenuUIView: IAdditiveUISceneStateの実装に加え、\n閉じるボタンが押されたタイミングなどポップアップUI固有の実装を必要に応じて外部に公開する。\n表示データは外部からPopupMenuUIView.EntryInfoを受け取る。

class "PopupMenuUIView.EntryInfo\nPopupMenuUIViewを表示するために必要なデータ" as PopupMenuUIView.EntryInfo {
  + string: itemName
  + int: amount
  + bool: canIncrement
  + bool: canDecrement
}

PopupMenuUIView -right-> PopupMenuUIView.EntryInfo
IAdditiveUISceneState <|.. PopupMenuUIView

```

```puml
title UI部品シーンとデータ連携の実装

package Presenter #4682b4 {

class "Presenter\n〇〇が押されたらデータを更新する等、モデルとUIの結びつけを行う。" as Presenter {
  + void Bind(UIManager.ViewSet view, IDataModel model)
}

}

package Model #bc8f8f {

interface "IDataModel\nUI画面に必要なデータ操作を定義したインターフェイス" as IDataModel {
  + void Initialize()
  + IObservable<bool>: onInitialized
  + string: itemName
  + int: amount
  + bool: canIncrement
  + bool: canDecrement
  + void UpdateAmount(int amount)
}

class "DummyDataModel\nIDataModelのダミー実装。デバッグ用に使用する。" as DummyDataModel {

}

class "ProductVersionDataMode\nIDataModelの本番実装" as ProductVersionDataModel {

}

}

class "UIManager\nUI画面に必要なUI一式とデータを取得・生成し、Presenterに流す" as UIManager {
    - Camera: _camera
    - ViewSet: _viewSet
    - IDataModel: _dataModel
}

package View#8fbc8f {

class "UIManager.ViewSet\nこのUI画面に必要なUI一式" as UIManager.ViewSet {
  + BaseUIView: baseUIView
  + PopupMenuUIView: popupUIView
  + InputblockView: inputBlockView
}

class BaseUIView {

}

class PopupMenuUIView {

}

class InputblockView {

}

}

IDataModel <|.down. DummyDataModel
IDataModel <|.down. ProductVersionDataModel
UIManager --> IDataModel
UIManager --> UIManager.ViewSet
UIManager --> Presenter
Presenter --> IDataModel
Presenter --> UIManager.ViewSet
UIManager.ViewSet --> BaseUIView
UIManager.ViewSet --> PopupMenuUIView
UIManager.ViewSet --> InputblockView


```