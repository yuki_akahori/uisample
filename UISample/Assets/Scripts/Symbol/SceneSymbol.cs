﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// シーンシンボル名
/// </summary>
public class SceneSymbol
{
    public const string BaseUIScene = "BaseUIScene";
    public const string PopupMenuUIScene = "PopupMenuUIScene";
    public const string InputBlockView = "InputBlockView";
    public const string ManagerScene = "ManagerScene";
}