﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ユーザーの入力をブロックするために最前面に表示されるUIビュー
///
/// スマートポインタみたいに、入力ブロックUIを使用しているものが一つでもある場合に表示され、誰も使っていなくなれば消える。
/// </summary>
public class InputblockView : MonoBehaviour
{
	void Awake()
	{
		// 必要になったら表示されるものなので最初は非表示
		gameObject.SetActive(false);
	}

	/// <summary>
	/// 入力ブロックUIを使用する
	/// </summary>
	public void Use()
	{
		++_counter;
		if(!gameObject.activeSelf)
		{
			gameObject.SetActive(true);
		}
	}

	/// <summary>
	/// 入力ブロックUIを使用を終了する
	/// </summary>
	public void Release()
	{
		--_counter;
		Debug.Assert(_counter >= 0);
		if(_counter == 0)
		{
			gameObject.SetActive(false);
		}
	}


	int _counter;
}
