﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// UI処理のユーティリティ
/// </summary>
public static class UIUtility
{
    /// <summary>
    /// 指定したシーンのルートに配置されたオブジェクトからコンポーネントを取得します。
    /// 複数コンポーネントが設定されている場合、一番最初のものを返します。
    /// </summary>
    /// <param name="sceneName">シーン名</param>
    /// <typeparam name="T">取得したいコンポーネントタイプ</typeparam>
    /// <returns>コンポーネントが取得できない場合、nullを返します</returns>
    public static T GetComponentFromScene<T>(string sceneName) where T : Component
    {
        var scene = SceneManager.GetSceneByName(sceneName);
        Debug.AssertFormat(scene.IsValid(), "シーン {0} は有効ではありません", sceneName);

        var rootObjects = scene.GetRootGameObjects();
        for (var i = 0; i < rootObjects.Length; ++i)
        {
            var t = rootObjects[i].GetComponent<T>();
            if (t != null)
            {
                return t;
            }
        }

        Debug.LogAssertionFormat("シーン {0} には指定したコンポーネントが存在しません", sceneName);

        return null;
    }

    /// <summary>
    /// 指定したシーンのルートに存在するCanvasコンポーネントに指定したカメラをセットします。
    /// すでに設定されていたカメラは破棄されます。※エディタ編集用にセットしているカメラのためゲームには不要。
    /// </summary>
    /// <param name="sceneName"></param>
    /// <param name="camera"></param>
    public static void SetCanvasCamera(string sceneName, Camera camera)
    {
        var scene = SceneManager.GetSceneByName(sceneName);
        Debug.AssertFormat(scene.IsValid(), "シーン {0} は有効ではありません", sceneName);

        var rootObjects = scene.GetRootGameObjects();
        for (var i = 0; i < rootObjects.Length; ++i)
        {
            var canvas = rootObjects[i].GetComponent<Canvas>();
            if (canvas != null)
            {
                var destroyCam = canvas.worldCamera != null && canvas.worldCamera != camera;
                if (destroyCam)
                {
                    GameObject.DestroyImmediate(canvas.worldCamera.gameObject);
                }
                canvas.worldCamera = camera;
            }
        }
    }
}

