﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

/// <summary>
/// UIのエディタ用ユーティリティ
/// </summary>
public static class UIEditorUtility
{
    /// <summary>
    /// エディタ上に指定したシーンを読み込む
    /// </summary>
    /// <param name="sceneName">読み込むシーン名</param>
    public static void TryOpenScene(string sceneName)
    {
        var sceneInfo = EditorSceneManager.GetSceneByName(sceneName);
        if (!sceneInfo.isLoaded)
        {
            var scenePath = EditorBuildSettings.scenes.Where(
                scene => System.IO.Path.GetFileNameWithoutExtension(scene.path) == sceneName
            ).Select(scene => scene.path)
            .FirstOrDefault();

            Debug.AssertFormat(string.IsNullOrEmpty(scenePath) == false, "シーン名 {0} はビルド対象になっていません", sceneName);

            EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);
        }
    }

    /// <summary>
    /// 指定したシーンのルートに配置されたEditorという名前のオブジェクトからコンポーネントを取得します。
    /// </summary>
    /// <param name="sceneName">シーン名</param>
    /// <typeparam name="T">取得したいコンポーネントタイプ</typeparam>
    /// <returns>コンポーネントが取得できない場合、nullを返します</returns>
    public static T GetComponentInChildrenFromEditorObj<T>(string sceneName) where T : Component
    {
        var scene = SceneManager.GetSceneByName(sceneName);
        Debug.AssertFormat(scene.IsValid(), "シーン {0} は有効ではありません", sceneName);

        var rootObjects = scene.GetRootGameObjects();
        for (var i = 0; i < rootObjects.Length; ++i)
        {
            var rootObj = rootObjects[i];
            if (rootObj.name == "Editor")
            {
                var component = rootObj.GetComponentInChildren<T>();
                Debug.AssertFormat(component != null, "シーン {0} からコンポーネントを取得できませんでした。");
                return component;
            }
        }

        Debug.LogAssertionFormat("シーン {0} のルートにEditorという名前のオブジェクトが存在しません。", sceneName);
        return null;
    }
}

#endif