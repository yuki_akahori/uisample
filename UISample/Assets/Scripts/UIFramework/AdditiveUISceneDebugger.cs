﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using UniRx;

/// <summary>
/// 加算読込するUIシーンのデバッグ補助コンポーネント
/// 以下の機能を持つ
/// - EventSystemが存在しなければ作成する
/// - Viewの呼び出しをインスペクタ上から行えるようにする
/// </summary>
public class AdditiveUISceneDebugger : MonoBehaviour
{
    /// <summary>
    /// ビューを呼び出すのに必要なEntryInfoの供給インターフェイス
    /// </summary>
    public interface IEntryInfoProvider
    {
        /// <summary>
        /// ビューのEntryメソッドに渡す情報
        /// </summary>
        object entryInfo { get; }
    }

    void Awake()
    {
        // インターフェイス取得
        _additiveSceneState = _viewGameObject.GetComponent<IAdditiveUISceneState>();
        Debug.Assert(_additiveSceneState != null);

        _entryInfoProvider = _entryInfoProviderObject.GetComponent<IEntryInfoProvider>();
        Debug.Assert(_entryInfoProvider != null);

        // シーン登場完了・退場完了タイミングをInspector上で確認できるように
        _additiveSceneState.onEntryComplete.Subscribe(_ => _onEntryComplete = true).AddTo(this);
        _additiveSceneState.onExitComplete.Subscribe(_ => _onExitComplete = true).AddTo(this);

        // EventSystemが存在しない場合は作成
        if (EventSystem.current == null)
        {
            var instance = new GameObject("EventSystem");
            instance.transform.SetParent(transform);
            EventSystem.current = instance.AddComponent<EventSystem>();
            instance.AddComponent<StandaloneInputModule>();
        }
    }

    /// <summary>
    /// デバッグ対象のUI表示を開始する
    /// </summary>
    void EntryScene()
    {
        _onEntryComplete = false;
        _additiveSceneState.Entry(_entryInfoProvider.entryInfo);
    }

    /// <summary>
    /// デバッグ対象のUI表示を終了する
    /// </summary>
    void ExitScene()
    {
        _onExitComplete = false;
        _additiveSceneState.Exit();
    }

    [SerializeField, Tooltip("デバッグ対象のビューオブジェクト")]
    GameObject _viewGameObject;
    [SerializeField, Tooltip("ビューを呼び出す際のEntryInfoを供給するオブジェクト")]
    GameObject _entryInfoProviderObject;
#pragma warning disable 0414    // 変数未使用警告の抑制
    [SerializeField, Tooltip("デバッグ対象のUIビューの登場完了タイミング確認用")]
    bool _onEntryComplete;
    [SerializeField, Tooltip("デバッグ対象のUIビューの退場完了タイミング確認用")]
    bool _onExitComplete;
#pragma warning restore 0414

    IAdditiveUISceneState _additiveSceneState;
    IEntryInfoProvider _entryInfoProvider;

    /// <summary>
    /// インスペクタ拡張
    /// プレイモード中に対象のUIの登場・退場を行えるようにする
    /// </summary>
    [CustomEditor(typeof(AdditiveUISceneDebugger))]
    public class InspectorExtention : Editor
    {
        AdditiveUISceneDebugger _debugger;
        void OnEnable()
        {
            _debugger = (AdditiveUISceneDebugger)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (EditorApplication.isPlaying)
            {
                if (GUILayout.Button("UIシーン登場"))
                {
                    _debugger.EntryScene();
                }
                if (GUILayout.Button("UIシーン退場"))
                {
                    _debugger.ExitScene();
                }
            }
        }
    }
}
#endif
