﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// 加算UIシーンインターフェイス
/// </summary>
public interface IAdditiveUISceneState
{
    /// <summary>
    /// UIの表示開始を行う
    /// </summary>
    /// <param name="entryInfo">UIにわたす外部情報</param>
    void Entry(object entryInfo);

    /// <summary>
    /// UIの表示開始処理が完了したタイミングで発行されるイベント
    /// </summary>
    IObservable<Unit> onEntryComplete { get; }

    /// <summary>
    /// UIの表示終了を行う
    /// </summary>
    void Exit();

    /// <summary>
    /// UIの表示終了処理が完了したタイミングで発行されるイベント
    /// </summary>
    IObservable<Unit> onExitComplete { get; }
}
