﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

/// <summary>
/// PopupMenuUIシーンのUIビューコンポーネント
/// </summary>
public class PopupMenuUIView : MonoBehaviour, IAdditiveUISceneState
{

    /// <summary>
    /// PopMenuUIViewを表示するに当たり必要な情報
    /// </summary>
    [System.Serializable]
    public class EntryInfo
    {
        public EntryInfo(string itemName, int amount, bool canIncrement, bool canDecrement)
        {
            _itemName = itemName;
            _amount = amount;
            _canIncrement = canIncrement;
            _canDecrement = canDecrement;
        }

        public string itemName { get { return _itemName; } }
        public int amount { get { return _amount; } }
        public bool canIncrement { get { return _canIncrement; } }
        public bool canDecrement { get { return _canDecrement; } }

        [SerializeField, Tooltip("アイテム名")]
        string _itemName;
        [SerializeField, Tooltip("アイテム数")]
        int _amount;
        [SerializeField, Tooltip("アイテムを増やせるか")]
        bool _canIncrement;
        [SerializeField, Tooltip("アイテムを減らせるか")]
        bool _canDecrement;
    }

    void Start()
    {
        // 必要に応じて呼び出されるシーンなので最初は非表示にしておく
        gameObject.SetActive(false);
    }

    /// <summary>
    /// UIの表示開始を行う
    /// </summary>
    /// <param name="entryInfo">UIにわたす外部情報</param>
    public void Entry(object entryInfo)
    {
        Debug.AssertFormat(entryInfo is EntryInfo, "型が不正です");

        // UIのオブジェクトが無効になっているので有効に
        gameObject.SetActive(true);

        // 受け取ったデータを反映
        var myEntryInfo = (EntryInfo)entryInfo;
        _itemName = myEntryInfo.itemName;
        UpdateAmount(myEntryInfo.amount, myEntryInfo.canIncrement, myEntryInfo.canDecrement);

        // 適当な表示演出。とりあえずCanvasグループのアルファフェード
        StartCoroutine(EntrySequence());
    }

    /// <summary>
    /// UIの表示開始処理が完了したタイミングで発行されるイベント
    /// </summary>
    public IObservable<Unit> onEntryComplete { get { return _onEntryComplete; } }

    /// <summary>
    /// UIの表示終了を行う
    /// </summary>
    public void Exit()
    {
        // 適当な終了演出。とりあえずフェードアウト
        StartCoroutine(ExitSequence());
    }

    /// <summary>
    /// UIの表示終了処理が完了したタイミングで発行されるイベント
    /// </summary>
    public IObservable<Unit> onExitComplete { get { return _onExitComlete; } }

    /// <summary>
    /// アイテム数の更新
    /// </summary>
    /// <param name="amount">更新後のアイテム数</param>
    /// <param name="canIncrement">アイテム数を増やすことが可能か</param>
    /// <param name="canDecrement">アイテム数をへらすことが可能か</param>
    public void UpdateAmount(int amount, bool canIncrement, bool canDecrement)
    {
        _incrementButton.interactable = canIncrement;
        _decrementButton.interactable = canDecrement;
        _text.text = "アイテム名: " + _itemName + " 数: " + amount;
    }

    /// <summary>
    /// アイテム数を増やすボタンが押されたときに発行されるイベント
    /// </summary>
    public IObservable<Unit> onClickIncrement { get { return _incrementButton.OnClickAsObservable(); } }

    /// <summary>
    /// アイテム数をへらすボタンが押されたときに発行されるイベント
    /// </summary>
    public IObservable<Unit> onClickDecrement { get { return _decrementButton.OnClickAsObservable(); } }

    /// <summary>
    /// 閉じるボタンが押されたときに発行されるイベント
    /// </summary>
    public IObservable<Unit> onClickClose { get { return _closeButton.OnClickAsObservable(); } }

    /// <summary>
    /// UI表示演出
    /// </summary>
    IEnumerator EntrySequence()
    {
        // UIをフェードインさせる
        _canvasGroup.alpha = 0;
        gameObject.SetActive(true);

        var elapsedTime = 0.0f;
        bool busy = true;
        while (busy)
        {
            yield return null;

            elapsedTime += Time.deltaTime;
            busy = elapsedTime < _fadeDuration;
            _canvasGroup.alpha = busy ? (elapsedTime / _fadeDuration) : 1.0f;
        }

        // 表示開始完了イベントを発行
        _onEntryComplete.OnNext(Unit.Default);
    }

    /// <summary>
    /// UI退場演出
    /// </summary>
    IEnumerator ExitSequence()
    {
        // UIをフェードアウトさせる
        var elapsedTime = 0.0f;
        bool busy = true;
        while (busy)
        {
            yield return null;

            elapsedTime += Time.deltaTime;
            busy = elapsedTime < _fadeDuration;
            _canvasGroup.alpha = busy ? (1.0f - (elapsedTime / _fadeDuration)) : 0.0f;
        }

        // 表示が消えたのでキャンバスごと無効に
        gameObject.SetActive(false);

        // 表示終了完了イベントを発行
        _onExitComlete.OnNext(Unit.Default);
    }

    [SerializeField]
    CanvasGroup _canvasGroup;
    [SerializeField]
    Button _incrementButton;
    [SerializeField]
    Button _decrementButton;
    [SerializeField]
    Button _closeButton;
    [SerializeField]
    Text _text;
    [SerializeField]
    float _fadeDuration = 0.2f;

    Subject<Unit> _onEntryComplete = new Subject<Unit>();
    Subject<Unit> _onExitComlete = new Subject<Unit>();

    string _itemName;
}
