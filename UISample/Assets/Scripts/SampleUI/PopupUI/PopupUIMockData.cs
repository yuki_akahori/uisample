﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// PopUIシーンのUIをテスト表示するために必要なダミーデータ
/// </summary>
public class PopupUIMockData : MonoBehaviour, AdditiveUISceneDebugger.IEntryInfoProvider
{
    /// <summary>
    /// ビューのEntryメソッドに渡す情報
    /// </summary>
    public object entryInfo { get { return _entryInfo; } }

	[SerializeField]
	PopupMenuUIView.EntryInfo _entryInfo;
}

#endif
