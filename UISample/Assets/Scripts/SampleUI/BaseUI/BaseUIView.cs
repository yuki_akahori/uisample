﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

/// <summary>
/// BaseUIシーンのUIビューコンポーネント
/// </summary>
public class BaseUIView : MonoBehaviour, IAdditiveUISceneState
{
    /// <summary>
    /// BaseUIシーンのUIビューを表示するにあたり、必要な情報
    /// </summary>
    [System.Serializable]
    public class EntryInfo
    {
        public EntryInfo(string itemName, int amount)
        {
            _itemName = itemName;
            _amount = amount;
        }

        public string itemName { get { return _itemName; } }
        public int amount { get { return _amount; } }

        [SerializeField,]
        string _itemName;
        [SerializeField]
        int _amount;
    }

    /// <summary>
    /// UIの表示開始を行う
    /// </summary>
    /// <param name="entryInfo">UIにわたす外部情報</param>
    public void Entry(object entryInfo)
    {
        Debug.AssertFormat(entryInfo is EntryInfo, "型が不正です");
        var myEntryInfo = (EntryInfo)entryInfo;
        _itemName = myEntryInfo.itemName;
        _amount = myEntryInfo.amount;
        UpdateData();

        // 最初から表示されるシーンなので特に演出はしない、即時完了
        _onEntryComplete.OnNext(Unit.Default);
    }

    /// <summary>
    /// UIの表示開始処理が完了したタイミングで発行されるイベント
    /// </summary>
    public IObservable<Unit> onEntryComplete { get { return _onEntryComplete; } }

    /// <summary>
    /// UIの表示終了を行う
    /// </summary>
    public void Exit()
    {
        Debug.LogWarningFormat("BaseUIシーンは常に表示しているのでこのメソッドが呼び出されることはない。");
    }

    /// <summary>
    /// UIの表示終了処理が完了したタイミングで発行されるイベント
    /// </summary>
    public IObservable<Unit> onExitComplete
    {
        get
        {
            Debug.LogWarningFormat("BaseUIシーンは常に表示しているのでこのイベント購読は不要");
            return Observable.Return(Unit.Default);
        }
    }


    /// <summary>
    /// アイテム数を更新する
    /// </summary>
    /// <param name="amount">更新後のアイテム数</param>
    public void UpdateAmount(int amount)
    {
        _amount = amount;
        UpdateData();
    }

    /// <summary>
    /// メニューを開くボタンが押されたときに発行されるイベント
    /// </summary>
    /// <returns></returns>
    public IObservable<Unit> onClickOpenMenu { get { return _openMenuButon.OnClickAsObservable(); } }

    /// <summary>
    /// データをUIへ反映する
    /// </summary>
    void UpdateData()
    {
        _itemInfoText.text = "アイテム名: " + _itemName + " 数: " + _amount;
    }

    string _itemName;
    int _amount;

    [SerializeField]
    Text _itemInfoText;
    [SerializeField]
    Button _openMenuButon;

    Subject<Unit> _onEntryComplete = new Subject<Unit>();

}
