﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BaseUIシーンのUIをテスト表示するために必要なダミーデータ
/// </summary>
public class BaseUIMockData : MonoBehaviour, AdditiveUISceneDebugger.IEntryInfoProvider
{
    /// <summary>
    /// ビューのEntryメソッドに渡す情報
    /// </summary>
    public object entryInfo { get { return _entryInfo; } }

    [SerializeField]
    BaseUIView.EntryInfo _entryInfo;
}
#endif
