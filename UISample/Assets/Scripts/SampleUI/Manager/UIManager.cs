﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// UIのマネージャ
/// 必要なビューとモデルの生成・取得を行い、ビューとモデルをつなぎ合わせるプレゼンターに受け渡す。
/// </summary>
public class UIManager : MonoBehaviour
{
    /// <summary>
    /// 扱うUIビューをひとまとめにしたもの
    /// </summary>
    public class ViewSet
    {
        public ViewSet(BaseUIView baseUIView, PopupMenuUIView popupUIView, InputblockView inputBlockView)
        {
            this.baseUIView = baseUIView;
            this.popupUIView = popupUIView;
            this.inputBlockView = inputBlockView;
        }

        public BaseUIView baseUIView { get; private set; }
        public PopupMenuUIView popupUIView { get; private set; }
        public InputblockView inputBlockView { get; private set; }
    }

    /// <summary>
    /// 追加で読み込む必要のあるシーンたち
    /// </summary>
    public static readonly string[] AdditiveUIScenes = new string[]
    {
        SceneSymbol.BaseUIScene,
        SceneSymbol.PopupMenuUIScene,
        SceneSymbol.InputBlockView
    };

    void Start()
    {
        // 今回はエディタ上に必要なシーンをすべて読みこんだ状態で実行される前提。
        // なので、このタイミングで必要なシーンが全て読み込み完了されたときのメソッドを呼び出す。
        // 本当はシーンマネージャーみたいなものが降り、マネージャーのシーンロード完了イベント経由で呼び出すものです。
        OnSceneLoadComplety();
    }

    /// <summary>
    /// 必要なシーンがすべて読み込まれたときに呼び出される
    /// </summary>
    void OnSceneLoadComplety()
    {
        // 加算シーンのUIが使用するカメラをセットする
        UIUtility.SetCanvasCamera(SceneSymbol.BaseUIScene, _camera);
        UIUtility.SetCanvasCamera(SceneSymbol.PopupMenuUIScene, _camera);
        UIUtility.SetCanvasCamera(SceneSymbol.InputBlockView, _camera);

        // 加算シーンのなかから使用するビューを取得する
        var baseUIView = UIUtility.GetComponentFromScene<BaseUIView>(SceneSymbol.BaseUIScene);
        var popupUIView = UIUtility.GetComponentFromScene<PopupMenuUIView>(SceneSymbol.PopupMenuUIScene);
        var inputBlockView = UIUtility.GetComponentFromScene<InputblockView>(SceneSymbol.InputBlockView);
        _viewSet = new ViewSet(baseUIView, popupUIView, inputBlockView);

        // データモデルの初期化を開始する
        _dataModel = GetDataModel();

        _dataModel.onInitialized.Subscribe(OnInitializeDataModel).AddTo(this);
        _dataModel.Initialize();
    }

    /// <summary>
    /// データモデルが初期化完了したタイミングで呼び出される
    /// </summary>
    /// <param name="success">初期化に成功したか</param>
    void OnInitializeDataModel(bool success)
    {
        Debug.Assert(success);

        // モデルとビューをプレゼンターに渡して結びつけを行わせる
        var presenter = gameObject.AddComponent<Presenter>();
        presenter.Bind(_viewSet, _dataModel);
    }

    /// <summary>
    /// データモデルの取得
    /// </summary>
    IDataModel GetDataModel()
    {
#if UNITY_EDITOR
        // デバッグ用コンポーネントを取得。ダミーデータを使うのであればダミーのモデルを返す
        var debugComponent = UIEditorUtility.GetComponentInChildrenFromEditorObj<UIManagerDebugger>("ManagerScene");
        if (debugComponent.useDummyData)
        {
            return debugComponent.dummyDataModel;
        }
#endif

        return new ProductVersionDataModel(itemName: "本番アイテム名", amount: 10, maxNum: 20, minNum: 0);
    }

    [SerializeField]
    Camera _camera;

    ViewSet _viewSet;
    IDataModel _dataModel;
}
