﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// ダミーのデータモデル
/// サーバーから取得しない実装。UIのデバッグ用に使う。
/// </summary>
public class DummyDataModel : MonoBehaviour, IDataModel
{
    /// <summary>
    /// 初期化開始
    /// </summary>
    public void Initialize()
    {
        // ダミーデータなので何もすることはない。すぐに初期化完了イベントを発行する
        _onInitialized.OnNext(true);
    }

    /// <summary>
    /// 初期化完了したときに発行されるイベント
    /// 引数で初期化に成功したかどうかのフラグを受け取る
    /// </summary>
    public IObservable<bool> onInitialized { get { return _onInitialized; } }

    /// <summary>
    /// アイテム名
    /// </summary>
    public string itemName { get { return _itemName; } }

    /// <summary>
    /// アイテム数
    /// </summary>
    public int amount { get { return _amount; } }

    /// <summary>
    /// アイテム数の更新処理
    /// </summary>
    /// <param name="amount">更新後のアイテム数</param>
    public void UpdateAmount(int amount)
    {
        _amount = Mathf.Clamp(amount, _itemMin, _itemMax);
    }

    /// <summary>
    /// アイテム数を増やすことが可能か
    /// </summary>
    public bool canIncrement { get { return _amount < _itemMax; } }

    /// <summary>
    /// アイテム数を減らすことが可能化
    /// </summary>
    public bool canDecrement { get { return _amount > _itemMin; } }

    [SerializeField]
    string _itemName;

    [SerializeField]
    int _amount;

    [SerializeField, Tooltip("アイテム数の最小")]
    int _itemMin;

    [SerializeField, Tooltip("アイテム数の最大")]
    int _itemMax;

    Subject<bool> _onInitialized = new Subject<bool>();
}
