﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// 本番用データモデル
/// 製品のサーバーからデータを取ってくる。
/// ※今回はデータモデルの差し替えによってUIに流し込むデータを切り替えられる例の提示のため、サーバーから取得する実装ではない。
/// </summary>
public class ProductVersionDataModel : IDataModel
{
    public ProductVersionDataModel(string itemName, int amount, int maxNum, int minNum)
    {
        _itemName = itemName;
        _amount = amount;
        _itemMax = maxNum;
        _itemMin = minNum;
    }

    /// <summary>
    /// 初期化開始
    /// </summary>
    public void Initialize()
    {
        // 本来であればサーバーからデータをとってきたりする。
        // 今回は仮なのですぐに初期化完了とする
        _onInitialized.OnNext(true);
    }

    /// <summary>
    /// 初期化完了したときに発行されるイベント
    /// 引数で初期化に成功したかどうかのフラグを受け取る
    /// </summary>
    public IObservable<bool> onInitialized { get { return _onInitialized; } }

    /// <summary>
    /// アイテム名
    /// </summary>
    public string itemName { get { return _itemName; } }

    /// <summary>
    /// アイテム数
    /// </summary>
    public int amount { get { return _amount; } }

    /// <summary>
    /// アイテム数の更新処理
    /// </summary>
    /// <param name="amount">更新後のアイテム数</param>
    public void UpdateAmount(int amount)
    {
        _amount = Mathf.Clamp(amount, _itemMin, _itemMax);
    }

    /// <summary>
    /// アイテム数を増やすことが可能か
    /// </summary>
    public bool canIncrement { get { return _amount < _itemMax; } }

    /// <summary>
    /// アイテム数を減らすことが可能化
    /// </summary>
    public bool canDecrement { get { return _amount > _itemMin; } }

    string _itemName;
    int _amount;
    int _itemMin;
    int _itemMax;
    Subject<bool> _onInitialized = new Subject<bool>();
}
