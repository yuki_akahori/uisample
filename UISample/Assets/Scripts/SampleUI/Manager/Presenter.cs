﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// プレゼンター
/// ビューとモデルを結びつける
/// </summary>
public class Presenter : MonoBehaviour
{
    /// <summary>
    /// ビューとモデルを結びつける
    /// </summary>
    /// <param name="view">ビュー</param>
    /// <param name="model">モデル</param>
    public void Bind(UIManager.ViewSet view, IDataModel model)
    {
        _model = model;
        _view = view;

        // BaseUIは最初から表示しているものなので、表示開始
        view.baseUIView.Entry(new BaseUIView.EntryInfo(model.itemName, model.amount));

        // ポップアップの表示開始・表示終了が完了したら入力ブロックを開放
        view.popupUIView.onExitComplete.Subscribe(_ => view.inputBlockView.Release()).AddTo(this);
        view.popupUIView.onEntryComplete.Subscribe(_ => view.inputBlockView.Release()).AddTo(this);

        // ポップアップ表示の開始ボタンが押されたら表示する
        view.baseUIView.onClickOpenMenu.Subscribe(_ =>
        {
            // 入力ブロックビューを表示
            view.inputBlockView.Use();

            // ポップアップメニューを表示
            view.popupUIView.Entry(new PopupMenuUIView.EntryInfo(model.itemName, model.amount, model.canIncrement, model.canDecrement));

        }).AddTo(this);

        // ポップアップメニューのアイテム数増減イベントが押されたらアイテム数を更新し、データを反映
        view.popupUIView.onClickDecrement.Subscribe(_ => UpdateAmount(-1)).AddTo(this);
        view.popupUIView.onClickIncrement.Subscribe(_ => UpdateAmount(+1)).AddTo(this);

        // ポップアップメニューの閉じるボタンが押されたら閉じる。
        view.popupUIView.onClickClose.Subscribe(_ =>
        {
            // 入力ブロックビューを表示
            view.inputBlockView.Use();

            // ポップアップメニューを閉じる
            view.popupUIView.Exit();
        });
    }

    void UpdateAmount(int diff)
    {
        // データ更新
        _model.UpdateAmount(_model.amount + diff);

        // ビューに反映
        _view.baseUIView.UpdateAmount(_model.amount);
        _view.popupUIView.UpdateAmount(_model.amount, _model.canIncrement, _model.canDecrement);
    }

    UIManager.ViewSet _view;
    IDataModel _model;
}