﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// UIマネージャシーンのデバッグ用コンポーネント
/// 以下の機能を持つ
/// ・データモデルにダミーを使うかどうかの指定
/// ・ダミーの出たモデルの提供
/// </summary>
public class UIManagerDebugger : MonoBehaviour
{
    public bool useDummyData { get { return _useDummyData; } }
    public IDataModel dummyDataModel { get { return _dummyDataModel; } }

    [SerializeField, Tooltip("ダミーデータを使うか")]
    bool _useDummyData;
    [SerializeField, Tooltip("ダミーデータオブジェクト")]
    DummyDataModel _dummyDataModel;

    /// <summary>
    /// インスペクタ拡張
    /// </summary>
    [CustomEditor(typeof(UIManagerDebugger))]
    public class InspectorExtention : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("必要なシーンを読み込む"))
            {
                foreach (var sceneName in UIManager.AdditiveUIScenes)
                {
                    UIEditorUtility.TryOpenScene(sceneName);
                }
            }
        }
    }
}
#endif
