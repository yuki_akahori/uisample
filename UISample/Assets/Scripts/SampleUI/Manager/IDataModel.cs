﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// アイテムUIシーンで使用するデータモデルインターフェイス
/// </summary>
public interface IDataModel
{
    /// <summary>
    /// 初期化開始
    /// </summary>
    void Initialize();

    /// <summary>
    /// 初期化完了したときに発行されるイベント
    /// 引数で初期化に成功したかどうかのフラグを受け取る
    /// </summary>
    IObservable<bool> onInitialized { get; }

    /// <summary>
    /// アイテム名
    /// </summary>
    string itemName { get; }

    /// <summary>
    /// アイテム数
    /// </summary>
    int amount { get; }

    /// <summary>
    /// アイテム数の更新処理
    /// </summary>
    /// <param name="amount">更新後のアイテム数</param>
    void UpdateAmount(int amount);

    /// <summary>
    /// アイテム数を増やすことが可能か
    /// </summary>
    bool canIncrement { get; }

    /// <summary>
    /// アイテム数を減らすことが可能化
    /// </summary>
    bool canDecrement { get; }
}